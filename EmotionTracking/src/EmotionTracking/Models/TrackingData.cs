﻿using System;
using EmotionTracking.DTO;

namespace EmotionTracking.Models
{
    public class TrackingData
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Image { get; set; }
        public string Data { get; set; }
        public string DeviceId { get; set; }
        public string Version { get; set; }
        public DateTime DateCreated { get; set; }

        public TrackingData()
        {
            DateCreated = DateTime.Now;
        }

        public TrackingData(UploadData data)
        {
            Name = data.Name;
            Image = data.Image;
            Data = data.Data;
            DeviceId = data.DeviceId;
            Version = data.Version;

            DateCreated = DateTime.Now;
        }
    }
}
