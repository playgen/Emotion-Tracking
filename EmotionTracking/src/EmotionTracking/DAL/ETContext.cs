﻿using Microsoft.EntityFrameworkCore;
using EmotionTracking.Models;

namespace EmotionTracking.DAL
{
    public class ETContextFactory
    {
        private readonly string _connectionString;

        public ETContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ETContext Create()
        {
            var builder = new DbContextOptionsBuilder<ETContext>();
            builder.UseSqlServer(_connectionString);

            var context = new ETContext(builder.Options);

            var didCreate = context.Database.EnsureCreated();

            return context;
        }
    }

    public class ETContext : DbContext
    {
        internal ETContext(DbContextOptions<ETContext> options)
            : base(options)
        {

        }
        public DbSet<TrackingData> TrackingDatas { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TrackingData>().ToTable("TrackingData");
        }
    }
}
