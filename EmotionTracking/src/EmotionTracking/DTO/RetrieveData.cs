﻿using EmotionTracking.Models;

namespace EmotionTracking.DTO
{
    public class RetrieveData
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Data { get; set; }

        public RetrieveData(TrackingData data)
        {
            Name = data.Name;
            Image = data.Image;
            Data = data.Data;
        }
    }
}
