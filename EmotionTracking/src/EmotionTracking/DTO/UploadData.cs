﻿namespace EmotionTracking.DTO
{
    /// <summary>
    /// DTO to hold information used to upload new data
    /// </summary>
    public class UploadData
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Data { get; set; }
        public string DeviceId { get; set; }
        public string Version { get; set; }
    }
}
