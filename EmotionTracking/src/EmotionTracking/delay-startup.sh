#!/bin/bash

if [ -z ${ET_SQL_PORT_1433_TCP} ]; then
	echo "You must link this container with sql server first"
	exit 1
fi
        
until nc -w 1 -z et-sql 1433; do
	echo "$(date) - waiting for et-sql..."
	sleep 1
done

exec "$@"
