﻿using Microsoft.AspNetCore.Mvc;
using EmotionTracking.DAL;

namespace EmotionTracking.Controllers
{
    public class DbController : Controller
    {
        protected ETContextFactory ContextFactory;
        protected ETContext Context;

        protected DbController(ETContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
            Context = ContextFactory.Create();
        }
    }
}
