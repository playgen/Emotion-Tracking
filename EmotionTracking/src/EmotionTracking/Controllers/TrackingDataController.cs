﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using EmotionTracking.DAL;
using EmotionTracking.DTO;
using EmotionTracking.Extensions;
using EmotionTracking.Models;

namespace EmotionTracking.Controllers
{
    [Route("api/[controller]")]
    public class TrackingDataController : DbController
    {
        public TrackingDataController(ETContextFactory contextFactory) : base(contextFactory)
        {
        }

        // GET api/trackingData
        [HttpGet]
        public IEnumerable<RetrieveData> Get()
        {
            return Context.TrackingDatas.ToRetrieveDatas();
        }

        // GET api/trackingData/5
        [HttpGet("{id}")]
        public RetrieveData Get(int id)
        {
            var data = Context.TrackingDatas.FirstOrDefault(t => t.Id == id);
            if (data != null)
            {
                return data.ToRetrieveData();
            }
            else
            {
                throw new Exception($"Unable to find tracking data with Id: {id}");
            }
        }

        // GET api/trackingdata/for/smiling
        [HttpGet("for/{emotion}")]
        public IEnumerable<RetrieveData> Get(string emotion)
        {
            var data = Context.TrackingDatas.Where(t => t.Name == emotion).ToRetrieveDatas();
            if (data != null)
            {
                return data;
            }
            else
            {
                throw new Exception($"No tracking data found for emoton: {emotion}");
            }
        }

        // POST api/trackingData
        [HttpPost]
        public void Post([FromBody]UploadData value)
        {
            var data = new TrackingData (value);

            Context.TrackingDatas.Add(data);
            Context.SaveChanges();
        }

        // DELETE api/trackingData/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var data = Context.TrackingDatas.FirstOrDefault(d => d.Id == id);

            if (data != null)
            {
                Context.TrackingDatas.Remove(data);
                Context.SaveChanges();
            }
            else
            {
                throw new Exception($"No data found with id: {id}");
            }

        }
    }
}
