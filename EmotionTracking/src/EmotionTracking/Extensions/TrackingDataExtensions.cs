﻿using System.Collections.Generic;
using EmotionTracking.DTO;
using EmotionTracking.Models;

namespace EmotionTracking.Extensions
{
    public static class TrackingDataExtension
    {
        public static RetrieveData ToRetrieveData(this TrackingData trackingData)
        {
            return new RetrieveData(trackingData);
        }

        public static IEnumerable<RetrieveData> ToRetrieveDatas(this IEnumerable<TrackingData> trackingDatas)
        {
            var retrieveDatas = new List<RetrieveData>();

            foreach (var trackingData in trackingDatas)
            {
                retrieveDatas.Add(trackingData.ToRetrieveData());
            }

            return retrieveDatas;
        }
    }
}
